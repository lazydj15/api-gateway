#!/usr/bin/env bash

IMAGE_NAME=$1
PORT=$2
VERSION=$1

# S_NAME=$(netstat -ltnup | grep '::${PORT}' | tr -s ' ' | cut -d ' ' -f 7 | cut -d '/' -f 1)

S_NAME=$(docker ps | grep ${IMAGE_NAME} | awk '{print $1}')
if [ ! -z "${S_NAME}" ]; then
    docker stop $(docker ps | grep ${IMAGE_NAME} | awk '{print $1}')
fi

S_NAME=$(docker ps -a | grep ${IMAGE_NAME} | awk '{print $1}')
if [ ! -z "${S_NAME}" ]; then
    docker rm $(docker ps -a | grep ${IMAGE_NAME} | awk '{print $1}')
fi

S_NAME=$(docker images | grep ${IMAGE_NAME} | tr -s ' ' | cut -d ' ' -f 3)
if [ ! -z "${S_NAME}" ]; then
    docker rmi $(docker images | grep ${IMAGE_NAME} | tr -s ' ' | cut -d ' ' -f 3) -f
fi

docker build --tag=${IMAGE_NAME}:${VERSION} .
